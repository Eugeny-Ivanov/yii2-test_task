<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%give_book}}`.
 */
class m210126_125250_create_give_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%give_book}}', [
            'id' => $this->primaryKey(),
            'book_id' => $this->integer(),
            'employee_id' => $this->integer(),
            'customer_id' => $this->integer(),
            'date_give' => $this->integer(),
            'return_date' => $this->integer(),
        ]);
        $this->createIndex(
            'idx-book-book_id',
            'give_book',
            'book_id'
        );

        $this->addForeignKey(
            'fk-book-book_id',
            'give_book',
            'book_id',
            'book',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-employee-employee_id',
            'give_book',
            'employee_id'
        );

        $this->addForeignKey(
            'fk-employee-employee_id',
            'give_book',
            'employee_id',
            'employee',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-customer-customer_id',
            'give_book',
            'customer_id'
        );

        $this->addForeignKey(
            'fk-customer-customer_id',
            'give_book',
            'customer_id',
            'customer',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-book-book_id',
            'give_book'
        );

        $this->dropIndex(
            'idx-book-book_id',
            'give_book'
        );

        $this->dropForeignKey(
            'fk-employee-employee_id',
            'give_book'
        );

        $this->dropIndex(
            'idx-employee-employee_id',
            'give_book'
        );

        $this->dropForeignKey(
            'fk-customer-customer_id',
            'give_book'
        );

        $this->dropIndex(
            'idx-customer-customer_id',
            'give_book'
        );

        $this->dropTable('{{%give_book}}');
    }
}
