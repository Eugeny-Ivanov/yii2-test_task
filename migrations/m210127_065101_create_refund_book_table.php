<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%refund_book}}`.
 */
class m210127_065101_create_refund_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%state_book}}', [
            'id' => $this->primaryKey(),
            'state_name' => $this->string(255)->notNull(),
        ]);

        $this->createTable('{{%refund_book}}', [
            'id' => $this->primaryKey(),
            'give_id' => $this->integer()->notNull(),
            'refund_date' => $this->integer(),
            'employee_id' => $this->integer(),
            'state_book_id' => $this->integer()
        ]);
        $this->createIndex(
            'idx-give-give_id',
            'refund_book',
            'give_id'
        );

        $this->addForeignKey(
            'fk-give-give_id',
            'refund_book',
            'give_id',
            'give_book',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-erefund-book-employee_id',
            'refund_book',
            'employee_id'
        );

        $this->addForeignKey(
            'fk-refund-book-employee_id',
            'refund_book',
            'employee_id',
            'employee',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-state_book-state_book_id',
            'refund_book',
            'state_book_id'
        );

        $this->addForeignKey(
            'fk-state_book-state_book_id',
            'refund_book',
            'state_book_id',
            'state_book',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-give-give_id',
            'refund_book'
        );

        $this->dropIndex(
            'idx-give-give_id',
            'refund_book'
        );

        $this->dropForeignKey(
            'fk-refund-book-employee_id',
            'refund_book'
        );

        $this->dropIndex(
            'idx-refund-book-employee_id',
            'refund_book'
        );
        $this->dropForeignKey(
            'fk-state_book-state_book_id',
            'refund_book'
        );

        $this->dropIndex(
            'idx-state_book-state_book_id',
            'refund_book'
        );
        $this->dropTable('{{%refund_book}}');
        $this->dropTable('{{%state_book}}');
    }
}
