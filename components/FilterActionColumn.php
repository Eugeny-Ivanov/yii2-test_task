<?php


namespace app\components;


class FilterActionColumn extends \yii\grid\ActionColumn
{
    public $filterContent;

    /**
     * @return string
     */
    protected function renderFilterCellContent()
    {
        return $this->filterContent;
    }
}