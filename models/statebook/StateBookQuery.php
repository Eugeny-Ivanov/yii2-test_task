<?php

namespace app\models\statebook;

/**
 * This is the ActiveQuery class for [[StateBook]].
 *
 * @see StateBook
 */
class StateBookQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return StateBook[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return StateBook|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
