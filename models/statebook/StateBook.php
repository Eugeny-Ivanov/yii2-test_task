<?php

namespace app\models\statebook;

use Yii;

/**
 * This is the model class for table "state_book".
 *
 * @property int $id
 * @property string $state_name
 *
 * @property RefundBook[] $refundBooks
 */
class StateBook extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'state_book';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state_name'], 'required'],
            [['state_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state_name' => 'State Name',
        ];
    }

    /**
     * Gets query for [[RefundBooks]].
     *
     * @return \yii\db\ActiveQuery|RefundBookQuery
     */
    public function getRefundBooks()
    {
        return $this->hasMany(RefundBook::className(), ['state_book_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return StateBookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StateBookQuery(get_called_class());
    }
}
