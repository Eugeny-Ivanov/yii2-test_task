<?php

namespace app\models\customer;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\customer\Customer;

/**
 * CustomerSearch represents the model behind the search form of `app\models\customer\Customer`.
 */
class CustomerSearch extends Customer
{
public $with_books;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'with_books'], 'integer'],
            [['fullname','passport_series', 'passport_number'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customer::find()->joinWith(['give','give.refund']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if((int)$this->with_books === 1){
            $query->groupBy(['customer.id'])
                ->having('COUNT(give_book.id) != COUNT("refund_book"."id")')
                ->orderBy(['customer.fullname' => 'ASC']);
        }elseif ((int)$this->with_books === 2){
            $query->groupBy(['customer.id'])
                ->having('COUNT(give_book.id) = COUNT("refund_book"."id")')
                ->orderBy(['customer.fullname'=>'ASC']);

        }

        // grid filtering conditions
        $query->andFilterWhere([
            'customer.id' => $this->id,
        ]);

        $query->andFilterWhere(['ilike', 'customer.fullname', $this->fullname])
            ->andFilterWhere(['ilike', 'customer.passport_series', $this->passport_series])
            ->andFilterWhere(['ilike', 'customer.passport_number', $this->passport_number]);

        return $dataProvider;
    }
}
