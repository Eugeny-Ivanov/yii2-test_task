<?php

namespace app\models\customer;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $fullname
 * @property string $passport_series
 * @property string $passport_number
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'passport_series', 'passport_number'], 'required'],
            [['fullname'], 'string', 'max' => 255],
            [['passport_series'], 'string', 'max' => 4],
            [['passport_number'], 'string', 'max' => 6],
            [['passport_series', 'passport_number'], 'unique', 'targetAttribute' => ['passport_series', 'passport_number']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
            'passport_series' => 'Passport Series',
            'passport_number' => 'Passport Number',
        ];
    }

    public function getGive()
    {
        return $this->hasMany(\app\models\givebook\GiveBook::class, ['customer_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return CustomerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomerQuery(get_called_class());
    }
}
