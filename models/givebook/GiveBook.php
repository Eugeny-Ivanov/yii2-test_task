<?php

namespace app\models\givebook;

use Yii;

/**
 * This is the model class for table "give_book".
 *
 * @property int $id
 * @property int|null $book_id
 * @property int|null $employee_id
 * @property int|null $customer_id
 * @property int|null $date_give
 * @property int|null $return_date
 *
 * @property Book $book
 * @property Customer $customer
 * @property Employee $employee
 */
class GiveBook extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'give_book';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => '\yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => false,
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['book_id', 'employee_id', 'customer_id', 'date_give', 'return_date'], 'required'],
            [['book_id', 'employee_id', 'customer_id', 'date_give', 'return_date'], 'default', 'value' => null],
            [['book_id', 'employee_id', 'customer_id'], 'integer'],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\book\Book::class, 'targetAttribute' => ['book_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\customer\Customer::class, 'targetAttribute' => ['customer_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\employee\Employee::class, 'targetAttribute' => ['employee_id' => 'id']],
            [['date_give'], 'date',
                'format' => 'php:d-m-Y',
                'timestampAttribute' => 'date_give',
            ],
            [['return_date'], 'date',
                'format' => 'php:d-m-Y',
                'timestampAttribute' => 'return_date',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book_id' => 'Книга',
            'employee_id' => 'Сотрудник',
            'customer_id' => 'Клиент',
            'date_give' => 'Дата выдачи',
            'return_date' => 'Выдана до',
            'book.title' => 'Книга',
            'employee.fullname' => 'Сотрудник',
            'customer.fullname' => 'Клиент',
        ];
    }

    /**
     * Gets query for [[Book]].
     *
     * @return \yii\db\ActiveQuery|BookQuery
     */
    public function getBook()
    {
        return $this->hasOne(\app\models\book\Book::class, ['id' => 'book_id']);
    }

    /**
     * Gets query for [[Customer]].
     *
     * @return \yii\db\ActiveQuery|CustomerQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(\app\models\customer\Customer::class, ['id' => 'customer_id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery|EmployeeQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(\app\models\employee\Employee::class, ['id' => 'employee_id']);
    }

    /**
     * Gets query for [[RefundBook]].
     *
     * @return \yii\db\ActiveQuery|RefundBook
     */
    public function getRefund(){
        return $this->hasOne(\app\models\refundbook\RefundBook::class, ['give_id' => 'id']);

    }

    /**
     * {@inheritdoc}
     * @return GiveBookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GiveBookQuery(get_called_class());
    }
}
