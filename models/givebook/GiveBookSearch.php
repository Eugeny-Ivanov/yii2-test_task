<?php

namespace app\models\givebook;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\givebook\GiveBook;

/**
 * GiveBookSearch represents the model behind the search form of `app\models\givebook\GiveBook`.
 */
class GiveBookSearch extends GiveBook
{
    public $book;
    public $employee;
    public $customer;
    public $from_date_give;
    public $to_date_give;
    public $from_return_date;
    public $to_return_date;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'book_id', 'employee_id', 'customer_id'], 'integer'],
            [['book','employee','customer', 'date_give', 'return_date'], 'safe'],
            [['from_date_give'], 'date', 'timestampAttribute' => 'from_date_give', 'format' => 'php:d-m-Y'],
            [['to_date_give'], 'date', 'timestampAttribute' => 'to_date_give', 'format' => 'php:d-m-Y'],
            [['from_return_date'], 'date', 'timestampAttribute' => 'from_return_date', 'format' => 'php:d-m-Y'],
            [['to_return_date'], 'date', 'timestampAttribute' => 'to_return_date', 'format' => 'php:d-m-Y']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GiveBook::find()->joinWith(['book','employee','customer']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['book'] = [
            'asc' => ['book.title' => SORT_ASC],
            'desc' => ['book.title' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['employee'] = [
            'asc' => ['employee.fullname' => SORT_ASC],
            'desc' => ['employee.fullname' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['customer'] = [
            'asc' => ['customer.fullname' => SORT_ASC],
            'desc' => ['customer.fullname' => SORT_DESC],
        ];

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'give_book.id' => $this->id,
        ]);
        $query->andFilterWhere(['like', 'book.title', $this->book])
            ->andFilterWhere(['like', 'employee.fullname', $this->employee])
            ->andFilterWhere(['like', 'customer.fullname', $this->customer])
            ->andFilterWhere(['between', 'date_give', $this->from_date_give, $this->to_date_give])
            ->andFilterWhere(['between', 'return_date', $this->from_return_date, $this->to_return_date]);

        return $dataProvider;
    }
}
