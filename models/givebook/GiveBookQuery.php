<?php

namespace app\models\givebook;

/**
 * This is the ActiveQuery class for [[GiveBook]].
 *
 * @see GiveBook
 */
class GiveBookQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return GiveBook[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return GiveBook|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function onHands($selectedId){
        return $this->joinWith(['refund', 'book'])
            ->andWhere(['give_book.customer_id' => $selectedId])
            ->andWhere(['IS', 'refund_book.id', null]);
    }

}
