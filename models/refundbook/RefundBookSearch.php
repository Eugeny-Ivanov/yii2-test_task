<?php

namespace app\models\refundbook;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\refundbook\RefundBook;

/**
 * RefundBookSearch represents the model behind the search form of `app\models\refundbook\RefundBook`.
 */
class RefundBookSearch extends RefundBook
{
    public $from_refund_date;
    public $to_refund_date;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'state_book_id'], 'integer'],
            [['employee_id', 'refund_date', 'give_id'], 'string'],
            [['from_refund_date'], 'date', 'timestampAttribute' => 'from_refund_date', 'format' => 'php:d-m-Y'],
            [['to_refund_date'], 'date', 'timestampAttribute' => 'to_refund_date', 'format' => 'php:d-m-Y'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RefundBook::find()->joinWith(['give.book', 'give.employee', 'stateBook']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'refund_book.id' => $this->id,
            'state_book_id' => $this->state_book_id,
        ]);
        $query->andFilterWhere(['like', 'book.title', $this->give_id])
            ->andFilterWhere(['like', 'employee.fullname', $this->employee_id])
            ->andFilterWhere(['between', 'refund_date', $this->from_refund_date, $this->to_refund_date]);

        return $dataProvider;
    }
}
