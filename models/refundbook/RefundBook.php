<?php

namespace app\models\refundbook;

use Yii;

/**
 * This is the model class for table "refund_book".
 *
 * @property int $id
 * @property int $give_id
 * @property int|null $refund_date
 * @property int|null $employee_id
 * @property int|null $state_book_id
 *
 * @property Employee $employee
 * @property GiveBook $give
 * @property StateBook $stateBook
 */
class RefundBook extends \yii\db\ActiveRecord
{
    public $customer_id;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'refund_book';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => '\yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => false,
                'updatedAtAttribute' => false,
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['give_id'], 'required'],
            [['give_id', 'refund_date', 'employee_id', 'state_book_id'], 'default', 'value' => null],
            [['give_id', 'employee_id', 'state_book_id', 'customer_id'], 'integer'],
            [['refund_date'], 'date',
                'format' => 'php:d-m-Y',
                'timestampAttribute' => 'refund_date',
            ],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\employee\Employee::class, 'targetAttribute' => ['employee_id' => 'id']],
            [['give_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\givebook\GiveBook::class, 'targetAttribute' => ['give_id' => 'id']],
            [['state_book_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\statebook\StateBook::class, 'targetAttribute' => ['state_book_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\customer\Customer::class, 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'give_id' => 'Книга',
            'refund_date' => 'Дата возврата',
            'employee_id' => 'Сотрудник',
            'state_book_id' => 'Состояние',
        ];
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery|EmployeeQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(\app\models\employee\Employee::class, ['id' => 'employee_id']);
    }

    /**
     * Gets query for [[Give]].
     *
     * @return \yii\db\ActiveQuery|GiveBookQuery
     */
    public function getGive()
    {
        return $this->hasOne(\app\models\givebook\GiveBook::class, ['id' => 'give_id']);
    }

    /**
     * Gets query for [[StateBook]].
     *
     * @return \yii\db\ActiveQuery|StateBookQuery
     */
    public function getStateBook()
    {
        return $this->hasOne(\app\models\statebook\StateBook::class, ['id' => 'state_book_id']);
    }

    /**
     * {@inheritdoc}
     * @return RefundBookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefundBookQuery(get_called_class());
    }
}
