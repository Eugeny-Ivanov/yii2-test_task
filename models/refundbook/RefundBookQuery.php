<?php

namespace app\models\refundbook;

/**
 * This is the ActiveQuery class for [[RefundBook]].
 *
 * @see RefundBook
 */
class RefundBookQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return RefundBook[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return RefundBook|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
