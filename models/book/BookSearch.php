<?php

namespace app\models\book;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\book\Book;

/**
 * BookSearch represents the model behind the search form of `app\models\book\Book`.
 */
class BookSearch extends Book
{
    public $in_stock;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'receipt_date'], 'integer'],
            [['title', 'vendor_code', 'author', 'in_stock'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find()->joinWith(['give', 'give.refund']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if((int)$this->in_stock === 1){
            $query->groupBy(['book.id'])
                ->having('COUNT(give_book.id) = COUNT("refund_book"."id")')
                ->orderBy(['book.title'=>'ASC']);

        }elseif ((int)$this->in_stock === 2){
            $query->groupBy(['book.id'])
                ->having('COUNT(give_book.id) != COUNT("refund_book"."id")')
                ->orderBy(['book.title'=>'ASC']);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->andFilterWhere(['ilike', 'title', $this->title])
            ->andFilterWhere(['ilike', 'vendor_code', $this->vendor_code])
            ->andFilterWhere(['ilike', 'author', $this->author]);


        return $dataProvider;
    }
}
