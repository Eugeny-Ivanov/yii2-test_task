<?php

namespace app\models\book;

/**
 * This is the ActiveQuery class for [[Customer]].
 *
 * @see Employee
 */
class BookQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Book[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Book|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function inStock()
    {
        return $this->joinWith(['give', 'give.refund'])->groupBy(['book.id'])
            ->having('COUNT(give_book.id) = COUNT("refund_book"."id")')
            ->orderBy(['book.title' => 'ASC']);
    }

}
