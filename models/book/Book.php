<?php

namespace app\models\book;

/**
 * This is the model class for table "book".
 *
 * @property int $id
 * @property string $title
 * @property string $vendor_code
 * @property int $receipt_date
 * @property string $author
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => '\yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => false,
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'vendor_code', 'receipt_date', 'author'], 'required'],
            [['receipt_date'], 'date',
                'format' => 'php:d-m-Y',
                'timestampAttribute' => 'receipt_date',
            ],
            [['title', 'vendor_code', 'author'], 'string', 'max' => 255],
        ];
    }

    public function getGive()
    {
        return $this->hasMany(\app\models\givebook\GiveBook::class, ['book_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'vendor_code' => 'Артикул',
            'receipt_date' => 'Дата поступления',
            'author' => 'Автор',
        ];
    }

    public static function find()
    {
        return new BookQuery(get_called_class());
    }
}
