<?php

namespace app\models\employee;
use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property int $id
 * @property string $fullname
 * @property string $function
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'function'], 'required'],
            [['fullname', 'function'], 'string', 'max' => 255],
        ];
    }

    public function getBooks()
    {
        return $this->hasMany(\app\models\book\Book::class, ['id' => 'book_id'])
            ->viaTable('give_book', ['employee_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
            'function' => 'Function',
        ];
    }
    public static function find()
    {
        return new EmployeeQuery(get_called_class());
    }
}
