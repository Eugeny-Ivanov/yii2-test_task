<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\statebook\StateBook */

$this->title = 'Create State Book';
$this->params['breadcrumbs'][] = ['label' => 'State Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="state-book-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
