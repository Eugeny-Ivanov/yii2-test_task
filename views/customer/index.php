<?php
use app\components\FilterActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\customer\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Customer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'fullname',
            'passport_series',
            'passport_number',
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width:50px;'],
            ],
            ['class' => FilterActionColumn::class,
                'filterContent' => Html::a('Reset filter', [''], [
                    'class' => 'btn btn-primary', 'title' => 'Reset filter',
                ]),
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
