<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\customer\CustomerSearch */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs(<<<JS
var with_books;
var fullname;
$(document).on('change', '#customer-search input', function(e) {    
    switch ($(this).attr('name')) {
        case 'CustomerSearch[with_books]':
            with_books = $(this).val();
        break;
        case 'CustomerSearch[fullname]':
            fullname = $(this).val();
        break;           
    }
    $.pjax({
        timeout: 4000,
        url: $('#customer-search').attr('action'),
        container: '#list-view',
        fragment: '#list-view',
        data: {CustomerSearch: {with_books: with_books, fullname: fullname}},
   });    
});
JS
    , yii\web\View::POS_END);
?>

<div class="customer-search">
    <?php \yii\widgets\Pjax::begin(['id' => 'search-customer']); ?>
    <?php $form = ActiveForm::begin([
        'id' => 'customer-search',
        'action' => ['frontend'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'with_books')->radioList([1 => 'Yes', 2 => 'No'],
        [
            'uncheck' => null
        ]
    )->label('С книгами'); ?>

    <?= $form->field($model, 'fullname') ?>

    <?php ActiveForm::end(); ?>
    <?php yii\widgets\Pjax::end() ?>
    <div class="form-group">
        <?= Html::a('Reset', [''], ['class' => 'btn btn-primary', 'id' => 'reset-filter']) ?>
    </div>
</div>
