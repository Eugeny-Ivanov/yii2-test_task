<?php

use yii\helpers\Html;
?>
<div class="col-3">
    <h5>Клинет: <?= Html::encode($model->fullname) ?></h5>
    <h5>Книги:</h5>
    <?php if ($model->give) : ?>
        <ul>
            <?php
            $class = '';
            $label_state = '';
            foreach ($model->give as $give) :
                $book = $give->book->title;
                if ($give->refund) {
                    $label_state = 'сдана ' . \Yii::$app->formatter->asDate($give->return_date, 'php:d-m-Y');
                    $class = 'style="color:green"';
                } else {
                    if ($give->return_date < time()) {
                        $label_state = 'на руках, просрочена, срок сдачи ' . \Yii::$app->formatter->asDate($give->return_date, 'php:d-m-Y');
                        $class = 'style="color:red"';
                    } else {
                        $label_state = 'на руках, срок сдачи ' . \Yii::$app->formatter->asDate($give->return_date, 'php:d-m-Y');
                        $class = 'style="color:orange"';
                    }
                }
                ?>
                <li><?= $book ?> <span <?= $class ?>><?= $label_state ?></span></li>
            <?php endforeach; ?>
        </ul>
    <?php else: ?>
        <h5 class="books-not-found">(книги не выдавались)</h5>
    <?php endif; ?>
</div>