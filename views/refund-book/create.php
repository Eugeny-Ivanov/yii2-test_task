<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\refundbook\RefundBook */

$this->title = 'Create Refund Book';
$this->params['breadcrumbs'][] = ['label' => 'Refund Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="refund-book-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
