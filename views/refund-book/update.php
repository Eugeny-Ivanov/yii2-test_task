<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\refundbook\RefundBook */

$this->title = 'Update Refund Book: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Refund Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="refund-book-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
