<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\FilterActionColumn;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\refundbook\RefundBookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Refund Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="refund-book-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Принять книгу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'give_id',
                'value' => 'give.book.title',
            ],
            //'refund_date',
            ['attribute' => 'refund_date',
                'format' => ['date', 'php:d.m.Y'],
                'value' => 'refund_date',
                'filter' => DateRangePicker::widget([
                    'id' => 'refund_date',
                    'model' => $searchModel,
                    'attribute' => 'refund_date',
                    'value' => !empty(Yii::$app->request->queryParams['refund_date']) ? Yii::$app->request->queryParams['refund_date'] : null,
                    'convertFormat' => true,
                    'startAttribute' => 'from_refund_date',
                    'endAttribute' => 'to_refund_date',
                    'language' => 'ru',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' по ',
                        ],
                    ]
                ]),

            ],
            //'employee_id',
            [
                'attribute' => 'employee_id',
                'value' => 'give.employee.fullname',
            ],
            [
                'attribute' => 'state_book_id',
                'value' => 'stateBook.state_name',
                'filter'=>ArrayHelper::map(\app\models\statebook\StateBook::find()->asArray()->all(), 'id', 'state_name'),
            ],
            //'state_book_id',
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width:50px;'],
            ],
            ['class' => FilterActionColumn::class,
                'filterContent' => Html::a('Reset filter', [''], [
                    'class' => 'btn btn-primary', 'title' => 'Reset filter',
                ]),
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
