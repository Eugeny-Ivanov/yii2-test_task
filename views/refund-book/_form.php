<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use conquer\select2\Select2Widget;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\refundbook\RefundBook */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="refund-book-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'customer_id')->widget(
        Select2Widget::class,
        [
            'items' => ArrayHelper::map(\app\models\givebook\GiveBook::find()->joinWith(['customer'])->all(), 'customer.id', 'customer.fullname'),
            'options' => ['prompt' => 'Выбрать', 'id' => 'customer-id'],
        ]
    ); ?>
    <?= $form->field($model, 'give_id')->widget(DepDrop::class, [
        'options' => ['id' => 'give_id'],
        'pluginOptions' => [
            'depends' => ['customer-id'],
            'placeholder' => 'Select...',
            'url' => Url::to(['customer-list'])
        ]
    ]); ?>



    <?= $form->field($model, 'refund_date')->widget(\yii\jui\DatePicker::class, [
        'language' => 'ru',
        'dateFormat' => 'dd-MM-yyyy',
    ]) ?>

    <?= $form->field($model, 'employee_id')->widget(
        Select2Widget::class,
        [
            'items' => ArrayHelper::map(\app\models\employee\Employee::find()->all(), 'id', 'fullname'),
            'options' => ['prompt' => 'Выбрать'],
        ]
    ); ?>

    <?= $form->field($model, 'state_book_id')->widget(
        Select2Widget::class,
        [
            'items' => ArrayHelper::map(\app\models\statebook\StateBook::find()->all(), 'id', 'state_name'),
            'options' => ['prompt' => 'Выбрать'],
        ]
    ); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
