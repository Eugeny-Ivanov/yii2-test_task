<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\daterange\DateRangePicker;
use app\components\FilterActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\models\givebook\GiveBookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Give Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="give-book-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Выдать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'book',
                'value' => 'book.title', //relation name with their attribute
            ],

            [
                'attribute' => 'employee',
                'value' => 'employee.fullname', //relation name with their attribute
            ],

            [
                'attribute' => 'customer',
                'value' => 'customer.fullname', //relation name with their attribute
            ],

            ['attribute' => 'date_give',
                'format' => ['date', 'php:d.m.Y'],
                'filter' => DateRangePicker::widget([
                    'id' => 'date_give',
                    'model' => $searchModel,
                    'attribute' => 'date_give',
                    'value' => !empty(Yii::$app->request->queryParams['date_give']) ? Yii::$app->request->queryParams['date_give'] : null,
                    'convertFormat' => true,
                    'startAttribute' => 'from_date_give',
                    'endAttribute' => 'to_date_give',
                    'language' => 'ru',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' по ',
                        ],
                    ]
                ]),

            ],
            ['attribute' => 'return_date',
                'format' => ['date', 'php:d.m.Y'],
                'value' => 'return_date',
                'filter' => DateRangePicker::widget([
                    'id' => 'return_date',
                    'model' => $searchModel,
                    'attribute' => 'return_date',
                    'value' => !empty(Yii::$app->request->queryParams['return_date']) ? Yii::$app->request->queryParams['return_date'] : null,
                    'convertFormat' => true,
                    'startAttribute' => 'from_return_date',
                    'endAttribute' => 'to_return_date',
                    'language' => 'ru',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' по ',
                        ],
                    ]
                ]),

            ],
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width:50px;'],
            ],
            ['class' => FilterActionColumn::class,
                'filterContent' => Html::a('Reset filter', [''], [
                    'class' => 'btn btn-primary', 'title' => 'Reset filter',
                ]),
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
