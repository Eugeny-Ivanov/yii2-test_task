<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\givebook\GiveBook */

$this->title = 'Create Give Book';
$this->params['breadcrumbs'][] = ['label' => 'Give Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="give-book-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
