<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use conquer\select2\Select2Widget;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\givebook\GiveBook */
/* @var $form yii\widgets\ActiveForm */
//\app\models\book\Book::find()->in
?>

<div class="give-book-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=$form->field($model, 'book_id')->widget(
    Select2Widget::class,
    [
    'items'=>ArrayHelper::map(\app\models\book\Book::find()->inStock()->all(), 'id', 'title'),
         'options'=>['prompt'=>'Выбрать'],
    ]
    ); ?>

    <?=$form->field($model, 'employee_id')->widget(
        Select2Widget::class,
        [
            'items'=>ArrayHelper::map(\app\models\employee\Employee::find()->all(), 'id', 'fullname'),
            'options'=>['prompt'=>'Выбрать'],
        ]
    ); ?>

    <?=$form->field($model, 'customer_id')->widget(
        Select2Widget::class,
        [
            'items'=>ArrayHelper::map(\app\models\customer\Customer::find()->all(), 'id', 'fullname'),
            'options'=>['prompt'=>'Выбрать'],
        ]
    ); ?>
    <?= $form->field($model, 'date_give')->widget(\yii\jui\DatePicker::class, [
        'id'=>'date_give',
        'language' => 'ru',
        'dateFormat' => 'dd-MM-yyyy',
    ]) ?>

    <?= $form->field($model, 'return_date')->widget(\yii\jui\DatePicker::class, [
        'id'=>'return_date',
            'language' => 'ru',
        'dateFormat' => 'dd-MM-yyyy',
    ]) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
