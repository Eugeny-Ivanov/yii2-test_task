<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\book\BookSearch */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs(<<<JS
var in_stock;
var title;
$(document).on('change', '#book-search input', function(e) {
    switch ($(this).attr('name')) {
        case 'BookSearch[in_stock]':
            in_stock = $(this).val();
        break;
        case 'BookSearch[title]':
            title = $(this).val();
        break;
    }
    $.pjax({
        timeout: 4000,
        url: $('#book-search').attr('action'),
        container: '#list-view',
        fragment: '#list-view',
        data: {BookSearch: {in_stock: in_stock, title: title}},
   });    
});
JS
    , yii\web\View::POS_END);
?>

<div class="book-search">
    <?php \yii\widgets\Pjax::begin(['id' => 'search']); ?>
    <?php $form = ActiveForm::begin([
        'id' => 'book-search',
        'action' => ['frontend'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>
    <?= $form->field($model, 'in_stock')->radioList([1 => 'Yes', 2 => 'No'], ['uncheck' => null]
    )->label('В наличии'); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php ActiveForm::end(); ?>
    <?php yii\widgets\Pjax::end() ?>
    <div class="form-group">
        <?= Html::a('Reset', [''], ['class' => 'btn btn-primary', 'id'=>'reset-filter']) ?>
    </div>
</div>
