<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-sm-3"><h4>Фильтр: </h4><?= $this->render('_search', ['model' => $searchModel]); ?></div>
    <div class="col-sm-9">
        <?php Pjax::begin(['timeout' => 30000, 'clientOptions' => ['container' => 'pjax-container']]); ?>
        <h4>Список</h4>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'item',
            'layout' => "<div class='lst row'>{items}</div><div>{pager}</div>",
            'options' => [
                'id' => 'list-view',
            ],
            'itemOptions' => [
                'tag' => 'div',
                'class' => 'col-lg-3 col-md-6 mb-3 book-list-item',
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
